import jenkins.model.*

def jenkinsLocationConfiguration = JenkinsLocationConfiguration.get()

jenkinsLocationConfiguration.setAdminAddress("noreply <jenkins-noreply@example.com>")

jenkinsLocationConfiguration.save()